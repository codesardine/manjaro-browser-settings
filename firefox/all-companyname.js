// Home page :
// https://developer.mozilla.org/en-US/docs/Mozilla/Preferences/A_brief_guide_to_Mozilla_preferences#Modifying_preferences
pref('browser.startup.homepage', "https://manjaro.org/");
pref('media.ffmpeg.vaapi-drm-display.enabled', true);
pref('browser.ssb.enabled', true);

